# use with docker
run:
	docker-compose up --build # use -d in production

down:
	docker-compose down

collect_static:
	docker exec django-skeleton_app_1 python3 manage.py collectstatic

# use with virtualenv
runserver:
	python app/manage.py runserver

makemigrations:
	python app/manage.py makemigrations

migrate:
	python app/manage.py migrate

test:
	python app/manage.py test

coverage:
	coverage run app/manage.py test
	coverage report