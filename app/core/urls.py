from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from apps.accounts.api.viewsets import UserViewSet, AuthViewSet
from apps.reports.api.viewsets import LabelViewSet, IssueViewSet

admin.site.site_header = 'Administração Tertúlia'
admin.site.site_title = 'Tertúlia'
admin.site.index_title = 'Página Inicial'

router = routers.DefaultRouter()
router.register(r'accounts', UserViewSet, basename='accounts')
router.register(r'auth', AuthViewSet, basename='auth')
router.register(r'labels', LabelViewSet, basename='labels')
router.register(r'issues', IssueViewSet, basename='issues')

urlpatterns = [
    path('', include('apps.pages.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('accounts/', include('apps.accounts.urls')),
    path('issues/', include('apps.reports.urls')),
]
