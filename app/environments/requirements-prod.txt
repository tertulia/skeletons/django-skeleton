-r requirements-base.txt
gunicorn==20.0.4
psycopg2==2.8.6
# redis==3.5.3  TODO remove commentation when using redis
# django-redis==4.12.1
