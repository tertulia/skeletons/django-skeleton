from django.shortcuts import render, redirect


def index(request):
    if request.user.is_authenticated:
        return redirect('home')
    return render(request, 'pages/index.html')


def home(request):
    if request.user.is_authenticated:
        return render(request, 'pages/home.html')
    return redirect('index')
