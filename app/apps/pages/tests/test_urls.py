from django.test import SimpleTestCase
from django.urls import reverse, resolve

from ..views import index, home


class TestUrls(SimpleTestCase):
    def test_index_url_resolves(self):
        url = reverse('index')
        self.assertEqual(resolve(url).func, index)

    def test_home_url_resolve(self):
        url = reverse('home')
        self.assertEqual(resolve(url).func, home)
