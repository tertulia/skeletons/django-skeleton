from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse


class TestIndexViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('index')

    def test_index_template(self):
        response = self.client.get(self.index_url)
        self.assertTemplateUsed(response, 'pages/index.html')


class TestHomeView(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.home_url = reverse('home')
        self.user = get_user_model().objects.create_user(email='user@email.com', password='some pass', name='user name')

    def test_home_template_unauthenticated_user(self):
        response = self.client.get(self.home_url)
        self.assertRedirects(response, '/')

    def test_home_template_authenticated_user(self):
        self.client.force_login(self.user)
        response = self.client.get(self.home_url)
        self.assertTemplateUsed(response, "pages/home.html")
