from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse

from .manager import IssueManager


class Label(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class Issue(models.Model):
    informer = models.ForeignKey(get_user_model(), on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=150)
    description = models.TextField()
    creation_date = models.DateField(auto_now_add=True)
    STATUS = [
        (0, 'closed'),
        (1, 'open')
    ]
    status = models.SmallIntegerField(choices=STATUS, default=1)
    was_reported = models.BooleanField(default=False)

    labels = models.ManyToManyField(Label)

    objects = IssueManager()

    def __str__(self):
        return self.title

    def short_description(self):
        return self.description[:20]

    def get_absolute_url(self):
        return reverse('detail-issue', args=[str(self.id)])
