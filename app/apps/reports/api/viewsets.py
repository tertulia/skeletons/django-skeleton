from rest_framework import viewsets, mixins
from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from ..models import Label, Issue


class LabelViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    authentication_classes = (TokenAuthentication,)
    queryset = Label.objects.all()
    permission_classes = (IsAuthenticated,)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Label
            fields = ["id", "title"]

    def get_serializer_class(self):
        return self.OutputSerializer


class IssueViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )

    class InputOutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Issue
            fields = ["title", "description", "informer", "labels"]

    def get_serializer_class(self):
        return self.InputOutputSerializer

    def perform_create(self, serializer):
        serializer.save(informer=self.request.user)
