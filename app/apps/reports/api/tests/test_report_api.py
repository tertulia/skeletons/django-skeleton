from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from ...models import Label, Issue


class APIBaseTestCase(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com', name='name', password='pass123')
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.labels = [Label.objects.create(title=t) for t in 'title']


class TestLabelApi(APIBaseTestCase):
    endpoint = reverse('labels-list')

    def test_get_list_labels_success(self):
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data,
                         [{"title": label.title, "id": label.id}
                          for label in self.labels])

    def test_not_allowed_methods(self):
        post_response = self.client.post(self.endpoint)
        self.assertEqual(post_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        put_response = self.client.put(self.endpoint)
        self.assertEqual(put_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        delete_response = self.client.delete(self.endpoint)
        self.assertEqual(delete_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthenticated_client_get_labels(self):
        unauthenticated_client = APIClient()
        response = unauthenticated_client.get(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestReportAPI(APIBaseTestCase):
    endpoint = reverse('issues-list')

    def setUp(self) -> None:
        super(TestReportAPI, self).setUp()
        self.payload = dict(description="this is an issue",
                            title="the title issue",
                            labels=[label.id for label in self.labels])
        self.patcher = mock.patch("apps.reports.manager.GitlabAPI.post_issue")
        self.mock_post_issue = self.patcher.start()

    def tearDown(self) -> None:
        super(TestReportAPI, self).tearDown()
        self.patcher.stop()

    def test_create_issue_success(self):
        response = self.client.post(self.endpoint, self.payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['description'], self.payload['description'])
        issue = Issue.objects.filter(title=self.payload['title'])
        self.assertTrue(issue.exists())

    def test_un_allowed_methods(self):
        get_response = self.client.get(self.endpoint)
        self.assertEqual(get_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        delete_response = self.client.delete(self.endpoint)
        self.assertEqual(delete_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        put_response = self.client.put(self.endpoint)
        self.assertEqual(put_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_report_issue_fail(self):
        self.payload.pop('labels')

        response = self.client.post(self.endpoint, self.payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
