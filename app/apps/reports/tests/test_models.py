import datetime
from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Label, Issue


class BaseTestCase(TestCase):
    def sample_label(self, title):
        return Label.objects.create(title=title)

    def sample_issue(self, informer: get_user_model(),
                     title: str = 'report',
                     description: str = 'nice description',
                     labels: list = []):
        issue = Issue.objects.create(informer=informer, title=title, description=description)
        for label in labels:
            issue.labels.add(label)
        issue.save()

        return issue

    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(name='user name', email='user@email.com', password='pass123')
        self.mock_gitlab_api = mock.patch('apps.reports.manager.GitlabAPI.post_issue', return_value=None)
        self.mock_post_issue = self.mock_gitlab_api.start()
        self.mock_post_issue.return_value = None

    def tearDown(self) -> None:
        self.mock_gitlab_api.stop()


class TestReportModel(BaseTestCase):
    def test_create_create_report_success(self):
        labels = [self.sample_label(title) for title in 'title']
        report = self.sample_issue(informer=self.user, labels=labels)
        self.assertEqual(report.creation_date, datetime.datetime.today().date())
        self.assertListEqual(labels, [*report.labels.all()])

    def test_report_representation(self):
        report = self.sample_issue(self.user)
        self.assertEqual(str(report), report.title)

    def test_deletion_informer_no_effect_report(self):
        report = self.sample_issue(self.user)
        self.user.delete()
        self.assertTrue(Issue.objects.filter(id=report.id).exists(), 'deletion of user must not effect the report')


class TestLabelModel(BaseTestCase):
    def test_label_representation(self):
        label = Label.objects.create(title='label title')
        self.assertEqual(str(label), label.title)
