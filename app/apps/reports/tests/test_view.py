from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from ..models import Label, Issue


class BaseTestCase(TestCase):
    def sample_user(self, name='user name', email='user@email.com', password='pass123'):
        return get_user_model().objects.create_user(name=name, email=email, password=password)

    def sample_label(self, title):
        return Label.objects.create(title=title)

    def sample_issue(self, informer: get_user_model(),
                     title: str = 'report',
                     description: str = 'nice description',
                     labels: list = []):
        issue = Issue.objects.create(informer=informer, title=title, description=description)
        if labels:
            issue.labels.add(*labels)
            issue.save()
        return issue

    def setUp(self) -> None:
        self.mock_gitlab_api = mock.patch('apps.reports.manager.GitlabAPI.post_issue', return_value=None)
        self.mock_post_issue = self.mock_gitlab_api.start()
        self.mock_post_issue.return_value = None

        self.user = self.sample_user()
        self.client.force_login(self.user)
        [self.sample_label(title=title) for title in 'title']
        self.labels = Label.objects.all()
        self.issue = self.sample_issue(self.user, labels=self.labels)

    def tearDown(self) -> None:
        self.mock_gitlab_api.stop()


class TestIssueListView(BaseTestCase):
    url = reverse('list-issue')

    def test_list_issues_by_unauthenticated_client(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.url)
        self.assertRedirects(response, '/accounts/login?next=' + self.url)

    def test_issue_list_view_templates_used(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'reports/issue_list.html')

    def test_issues_and_labels_in_context(self):
        response = self.client.get(self.url)
        self.assertIn(self.issue, response.context['object_list'])
        self.assertListEqual([*self.issue.labels.all()],
                             [*response.context['object_list'][0].labels.all()])

    def test_only_issue_informer_in_context(self):
        other_informer_issue = self.sample_issue(self.sample_user(email='other@informer.com'))
        response = self.client.get(self.url)
        self.assertNotIn(other_informer_issue, response.context['object_list'])


class TestIssueCreateView(BaseTestCase):
    url = reverse('create-issue')

    def test_create_issue_by_unauthenticated_client(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.post(self.url)
        self.assertRedirects(response, '/accounts/login?next=' + self.url)

    def test_issue_create_view_templates_used(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'reports/issue_create.html')

    def test_create_issue_success(self):
        payload = dict(title='a new issue report',
                       description="a good and nice description",
                       labels=[label.id for label in self.labels])
        self.client.post(self.url, payload)
        issue = Issue.objects.filter(title=payload['title']).first()
        self.assertIsNotNone(issue)
        self.assertEqual(issue.informer, self.user)


class TestIssueDetailView(BaseTestCase):
    def get_url(self, issue):
        return reverse('detail-issue', args=[issue.id])

    def test_issue_detail_view_template(self):
        url = self.get_url(self.issue)
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'reports/issue_detail.html')

    def test_issue_detail_by_unauthenticated_client(self):
        unauthenticated_client = Client()
        url = self.get_url(self.issue)
        response = unauthenticated_client.get(url)
        self.assertRedirects(response, '/accounts/login?next=' + url)

    def test_issue_detail_by_unauthorized_client(self):
        unauthorized_user = self.sample_user(email='other@user.com')
        unauthorized_client = Client()
        unauthorized_client.force_login(unauthorized_user)

        url = self.get_url(self.issue)
        response = unauthorized_client.get(url)
        self.assertEqual(response.status_code, 403)
