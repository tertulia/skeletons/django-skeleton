from django.test import SimpleTestCase
from django.urls import reverse, resolve

from .. import views


class TestUrls(SimpleTestCase):
    def test_create_issue_url(self):
        url = reverse('create-issue')
        self.assertEqual(resolve(url).func.view_class, views.IssueCreateView)

    def test_list_issue_url(self):
        url = reverse('list-issue')
        self.assertEqual(resolve(url).func.view_class, views.IssueListView)

    def test_detail_issue_url(self):
        issue_id = 1
        url = reverse('detail-issue', args=[issue_id])
        self.assertEqual(resolve(url).func.view_class, views.IssueDetailView)
