from django.urls import path

from . import views

urlpatterns = [
    path('', views.IssueListView.as_view(), name='list-issue'),
    path('create/', views.IssueCreateView.as_view(), name='create-issue'),
    path('<int:pk>/', views.IssueDetailView.as_view(), name='detail-issue'),
]
