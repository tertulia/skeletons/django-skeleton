from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views import generic

from .models import Issue


class IssueCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/accounts/login'
    template_name = 'reports/issue_create.html'

    model = Issue
    fields = ['title', 'description', 'labels']

    def form_valid(self, form):
        form.instance.informer = self.request.user
        return super(IssueCreateView, self).form_valid(form)


class IssueListView(LoginRequiredMixin, generic.ListView):
    model = Issue
    login_url = '/accounts/login'
    template_name = 'reports/issue_list.html'

    def get_queryset(self):
        return self.model.objects.filter(informer=self.request.user).order_by('-creation_date')


class IssueDetailView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    login_url = '/accounts/login'
    model = Issue
    fields = ['title', 'description', 'labels']

    def test_func(self):
        return self.request.user == self.get_object().informer
