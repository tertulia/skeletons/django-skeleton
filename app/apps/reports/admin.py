from django.contrib import admin

from .models import Issue, Label

admin.site.register(Issue)
admin.site.register(Label)
