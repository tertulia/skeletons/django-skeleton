from django.db import models

from .gitlab_integration.gitlab_api import GitlabAPI


class IssueManager(models.Manager):
    def create(self, title, description, informer=None, labels=None, **kwargs):
        if not informer:
            raise ValueError("Issue must have an informer")
        if not description:
            raise ValueError("Issue must have an description")
        issue = self.model(title=title, description=description, informer=informer, **kwargs)
        if labels:
            issue.labels.add(*labels)

        issue.save(using=self._db)
        gitlab_api = GitlabAPI(issue)
        gitlab_api.post_issue()
        return issue
