from unittest import mock

from django.test import SimpleTestCase
from rest_framework import status
from rest_framework.response import Response

from ..gitlab_api import GitlabAPI


class TestIssuesAPI(SimpleTestCase):
    def setUp(self) -> None:

        self.mock_issue = mock.Mock()
        self.mock_issue.description = 'this is an issue'
        self.mock_issue.title = 'issue title'
        self.mock_label = mock.Mock()
        self.mock_label.title = 'title'
        self.mock_issue.labels.all.return_value = [self.mock_label]

        self.gitlab_api = GitlabAPI(self.mock_issue)

        self.mock_request = mock.patch('apps.reports.gitlab_integration.gitlab_api.requests.post', return_value=None)
        self.mock_post = self.mock_request.start()

    def tearDown(self) -> None:
        self.mock_request.stop()

    def test_report_repository_headers(self):
        expected_headers = {"Content-type", "PRIVATE-TOKEN"}
        self.assertSetEqual(expected_headers, set(self.gitlab_api.headers.keys()))

    def test_report_repository_payload(self):
        expected_fields = {"title", "description", "labels"}
        self.assertEqual(expected_fields, self.gitlab_api.payload.keys())

    def test_post_issue_success(self):
        self.mock_post.return_value = Response({'title': self.mock_issue.title,
                                                'description': self.mock_issue.description,
                                                'labels': [label.title for label in self.mock_issue.labels.all()]},
                                               status=status.HTTP_201_CREATED)

        response = self.gitlab_api.post_issue()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
