import json

import requests
from decouple import config


class GitlabAPI:
    gitlab_private_token = config('GITLAB_PRIVATE_TOKEN', default='')
    gitlab_project_id = config('GITLAB_PROJECT_ID', default='')
    default_labels = ['by user']

    def __init__(self, issue):
        self.issue = issue

    @property
    def headers(self):
        return {"Content-type": "application/json",
                "PRIVATE-TOKEN": self.gitlab_private_token}

    @property
    def project_api_url(self):
        return f"https://gitlab.com/api/v4/projects/{self.gitlab_project_id}/issues"

    @property
    def payload(self):
        return self._get_payload()

    def _get_payload(self):
        return dict(
            title=self.issue.title,
            description=self.issue.description,
            labels=[label.title for label in self.issue.labels.all()] + self.default_labels
        )

    def post_issue(self):
        response = requests.post(self.project_api_url,
                                 headers=self.headers,
                                 data=json.dumps(self.payload))
        return response
