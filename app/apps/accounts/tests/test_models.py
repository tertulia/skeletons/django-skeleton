from django.contrib.auth import get_user_model
from django.test import TestCase


class UserModelTests(TestCase):
    def create_user(self, **params):
        return get_user_model().objects.create_user(**params)

    def setUp(self) -> None:
        self.user_params = dict(
            email='test@tertulia.com',
            password='testpass',
            name='full user name'
        )

    def test_create_user_with_email_successful(self):
        """Test creating a new user with an email is successful"""
        user = self.create_user(**self.user_params)

        self.assertEqual(user.email, self.user_params['email'],
                         f"user.email must be '{self.user_params['email']}'")
        self.assertTrue(user.check_password(self.user_params['password']), "password must be ok")

    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""
        self.user_params['email'] = 'test@TERTULIA.COM'
        user = self.create_user(**self.user_params)

        self.assertEqual(user.email, self.user_params['email'].lower(),
                         "user.email must be normalized")

    def test_new_invalid_email(self):
        """Test creating a new user with no email raises error"""
        self.user_params['email'] = None
        with self.assertRaises(ValueError):
            self.create_user(**self.user_params)

    def test_user_with_existing_email(self):
        self.create_user(**self.user_params)
        with self.assertRaises(Exception) as context:
            self.create_user(**self.user_params)
        self.assertTrue('UNIQUE constraint failed: accounts_user.email' in str(context.exception),
                        'must rise that email must be unique')


class SpecialUserModelTests(TestCase):

    def test_create_superuser(self):
        """Test creating a new superuser"""
        superuser = get_user_model().objects.create_superuser(
            email='super@tertulia.com',
            name='admin name',
            password='testpass'
        )
        self.assertTrue(superuser.is_superuser, 'must return True for a superuser')
        self.assertTrue(superuser.is_staff, 'must return True for a superuser')
