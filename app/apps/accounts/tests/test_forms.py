from django.test import TestCase

from ..forms import AccountsRegisterForm, EditUserProfileForm


class UnitTestAccountRegisterForm(TestCase):
    def setUp(self) -> None:
        self.user_data = dict(
            email='user@email.com',
            name='user name',
            password1="testpass1",
            password2="testpass1"
        )

    def test_user_form_valid_data(self):
        form = AccountsRegisterForm(data=self.user_data)
        self.assertTrue(form.is_valid())

    def test_user_form_no_data(self):
        form = AccountsRegisterForm(data={})
        self.assertFalse(form.is_valid())

    def test_user_invalid_email(self):
        self.user_data['email'] = 'not an email'
        form = AccountsRegisterForm(data=self.user_data)
        self.assertFalse(form.is_valid())


class UnitTestEditProfileForm(TestCase):
    def setUp(self) -> None:
        self.user_data = dict(
            email='user@email.com',
            name='user name',
            password="testpass1"
        )

    def test_user_form_valid_data(self):
        form = EditUserProfileForm(data=self.user_data)
        self.assertTrue(form.is_valid())

    def test_user_form_no_data(self):
        form = EditUserProfileForm(data={})
        self.assertFalse(form.is_valid())

    def test_user_invalid_email(self):
        self.user_data['email'] = 'not an email'
        form = EditUserProfileForm(data=self.user_data)
        self.assertFalse(form.is_valid())
