from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from app.apps.accounts.api.viewsets import missing_fields


class APIBaseTesCase(TestCase):
    def sample_user(self, **params):
        """Create and return a sample user"""
        if not params.get('name', None):
            self.user_payload['email'] = params.get('email', self.user_payload['email'])
            return get_user_model().objects.create_user(**self.user_payload)
        return get_user_model().objects.create_user(**params)

    def setUp(self) -> None:
        self.user_payload = {
            'email': 'test@tertulia.com',
            'password': 'testpass1',
            'name': 'user name'
        }
        self.client = APIClient()


class AccountsApiTests(APIBaseTesCase):
    """Test the user API (public)"""
    url_accounts = reverse('accounts-list')

    def test_register_valid_user_success(self):
        """Test creating user with valid payload"""
        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED,
                         "status code must be 201 CREATED")

        user = get_user_model().objects.get(email=self.user_payload['email'])

        self.assertTrue(user.check_password(self.user_payload['password']),
                        "password must be ok")

    def test_fields_after_registration_user_success(self):
        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertNotIn('password', res.data,
                         "password must not be in res.data")
        self.assertIn('email', res.data, 'user email must be in the response')
        self.assertIn('name', res.data, 'user name must be in the response')
        self.assertIn('id', res.data, 'user id must be in the response')

    def test_register_user_missing_email(self):
        email = self.user_payload.pop('email')

        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         "must not create user with no email")

        self.assertFalse(get_user_model().objects.filter(email=email).exists(),
                         'user must not be created')

    def test_register_user_no_password(self):
        self.user_payload.pop('password')

        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         "must not create user with no password")

        self.assertFalse(get_user_model().objects.filter(email=self.user_payload['email']).exists(),
                         'user must not be created')

    def test_register_user_invalid_password(self):
        self.user_payload['password'] = 'testpass'

        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertFalse(get_user_model().objects.filter(email=self.user_payload['email']).exists(),
                         'user must not be created')

    def test_register_user_no_name(self):
        name = self.user_payload.pop('name')

        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         "must not create user with no name")

        self.assertFalse(get_user_model().objects.filter(name=name).exists())

    def test_register_existing_user(self):
        """Test creating a user that already exists fails"""
        self.sample_user(**self.user_payload)

        res = self.client.post(self.url_accounts, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         'status code must be 409 CONFLICT')
        err = ErrorDetail(string='usuário com este e-mail já existe.', code='unique')
        self.assertIn(err, res.data['email'], 'error must be returned')

    def test_authenticated_client_receive_user_list(self):
        user = self.sample_user(email='user1@email.com')
        self.sample_user(email='user2@email.com')
        self.sample_user(email='user3@email.com')
        self.sample_user(email='user4@email.com')

        self.client.force_authenticate(user)

        res = self.client.get(self.url_accounts)

        num_users = get_user_model().objects.all().count()

        self.assertEqual(num_users, len(res.data), 'users must be listed')

    def test_unauthenticated_client_denied_user_list(self):
        unauthenticated_client = APIClient()
        res = unauthenticated_client.get(self.url_accounts)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED, 'unauthenticated user must not access')

    def test_authenticated_client_retrieve_other_user(self):
        user = self.sample_user()
        other_user = self.sample_user(email='other@user.com')

        self.client.force_authenticate(user)
        detail_account_url = reverse('accounts-detail', args=[other_user.id])
        res = self.client.get(detail_account_url)

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_unauthenticated_client_retrieve_other_user(self):
        user = self.sample_user()
        unauthenticated_client = APIClient()

        detail_account_url = reverse('accounts-detail', args=[user.id])
        res = unauthenticated_client.get(detail_account_url)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_authenticated_client_update_fields(self):
        user = self.sample_user()
        self.client.force_authenticate(user)

        new_user_params = {'name': 'new name', 'email': 'other@emai.com', 'password': '153759@Bc'}
        detail_account_url = reverse('accounts-detail', args=[user.id])
        res = self.client.patch(detail_account_url, data=new_user_params)

        user.refresh_from_db()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(user.name, new_user_params['name'], 'name must be updated')
        self.assertEqual(user.email, new_user_params['email'], 'email must be updated')
        self.assertTrue(user.check_password(new_user_params['password']))

    def test_authenticated_client_update_id_field(self):
        user = self.sample_user()
        self.client.force_authenticate(user)

        new_user_params = {'id': 'new id'}
        detail_account_url = reverse('accounts-detail', args=[user.id])
        self.client.patch(detail_account_url, data=new_user_params)
        user.refresh_from_db()

        self.assertNotEqual(user.id, new_user_params['id'], 'name must not be updated')

    def test_unauthenticated_client_update_fields(self):
        user = self.sample_user()
        unauthenticated_client = APIClient()

        new_user_params = {'name': 'new name', 'email': 'other@emai.com'}
        detail_account_url = reverse('accounts-detail', args=[user.id])
        res = unauthenticated_client.patch(detail_account_url, data=new_user_params)
        user.refresh_from_db()

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED,
                         'unauthenticated user must not be able to update other user')
        self.assertNotEqual(user.name, new_user_params['name'], 'name must not be updated')
        self.assertNotEqual(user.email, new_user_params['email'], 'email must not be updated')

    def test_authenticated_client_delete_self(self):
        user = self.sample_user()
        self.client.force_authenticate(user)

        detail_account_url = reverse('accounts-detail', args=[user.id])
        res = self.client.delete(detail_account_url)
        fetched_user = get_user_model().objects.filter(email=user.email)

        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(fetched_user.exists(), 'user must be deleted')

    def test_authenticated_client_delete_other_user(self):
        user = self.sample_user(email='user@email.com')
        self.client.force_authenticate(user)

        other_user = self.sample_user(email='other@user.com')
        detail_other_account_url = reverse('accounts-detail', args=[other_user.id])

        res = self.client.delete(detail_other_account_url)
        fetched_user = get_user_model().objects.filter(email=other_user.email)

        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
        self.assertTrue(fetched_user.exists(), 'user must not be deleted')

    def test_unauthenticated_client_delete_user(self):
        user = self.sample_user()
        unauthenticated_client = APIClient()

        detail_account_url = reverse('accounts-detail', args=[user.id])
        res = unauthenticated_client.delete(detail_account_url)
        fetched_user = get_user_model().objects.filter(email=user.email)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertTrue(fetched_user.exists(), 'user must not be deleted')


class AuthApiTests(APIBaseTesCase):
    url_auth_login = reverse('auth-login')
    url_auth_logout = reverse('auth-logout')

    def setUp(self) -> None:
        super(AuthApiTests, self).setUp()
        self.user = self.sample_user()

    def test_login_success(self):
        """Test user login"""
        res = self.client.post(self.url_auth_login, self.user_payload)
        token, _ = Token.objects.get_or_create(user=self.user)

        self.assertEqual(res.status_code, status.HTTP_200_OK,
                         "status code must be 200 OK")
        self.assertIn('key', res.data, 'token key must be in response')
        self.assertEqual(token.key, res.data['key'])

    def test_login_wrong_password(self):
        self.user_payload['password'] = 'wrong pass'
        res = self.client.post(self.url_auth_login, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn('key', res.data, 'must not contain key in response')

    def test_login_wrong_email(self):
        self.user_payload['email'] = 'wrong@email.com'
        res = self.client.post(self.url_auth_login, self.user_payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn('key', res.data, 'must not contain key in response')

    def test_login_no_email(self):
        """Test make login no email"""
        payload = dict(password='testpass')
        res = self.client.post(self.url_auth_login, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         "status code must be 400 BAD REQUEST")

    def test_login_no_password(self):
        """Test make login no password"""
        payload = dict(email='test@tertulia.com')
        res = self.client.post(self.url_auth_login, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST,
                         "status code must be 400 BAD REQUEST")

    def test_logout(self):
        self.client.force_authenticate(self.user)
        Token.objects.create(user=self.user)

        res = self.client.get(self.url_auth_logout)

        fetch_token = Token.objects.filter(user=self.user)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertFalse(fetch_token.exists())

    def test_missing_fields(self):
        data = dict(a=1, b=2, c=4)
        fields = ['a', 'b', 'c']
        is_missing, _ = missing_fields(data, *fields)
        self.assertEqual(is_missing, False)

        fields.append('d')
        is_missing, field = missing_fields(data, *fields)
        self.assertEqual((is_missing, field), (True, 'd'))


class TestAccountsAPIsPath(TestCase):

    def test_path_accounts_routes(self):
        accounts_url = reverse('accounts-list')
        raw_accounts_url = '/api/accounts/'
        self.assertEqual(raw_accounts_url, accounts_url)

    def test_path_login_in_auth_router(self):
        auth_login = reverse('auth-login')
        raw_auth_login = '/api/auth/login/'
        self.assertEqual(raw_auth_login, auth_login)

    def test_path_logout_in_auth_router(self):
        accounts_url = reverse('auth-logout')
        raw_accounts_url = '/api/auth/logout/'
        self.assertEqual(raw_accounts_url, accounts_url)
