from django.contrib.auth import get_user_model, authenticate
from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import permissions
from . import serializers


class UserViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin,
                  mixins.ListModelMixin, mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    queryset = get_user_model().objects.all()
    authentication_classes = (TokenAuthentication,)
    serializer_class = serializers.AccountsSerializer

    def get_permissions(self):
        permission_classes = [permissions.UpdateOwnUser]
        if self.action != 'create':
            permission_classes.append(IsAuthenticated)
        return [p() for p in permission_classes]


class AuthViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):

    @action(methods=['post', ], detail=False)
    def login(self, request, *args, **kwargs):
        is_missing, field = missing_fields(request.data, 'password', 'email')
        if is_missing:
            return Response({f"{field}": "Campo obrigatório"}, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(email=request.data['email'],
                            password=request.data['password'])
        if user:
            token = Token.objects.create(user=user)
            return Response({'key': token.key})

        return Response({"message": "Credenciais inválidas"},
                        status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get', ], detail=False, permission_classes=[IsAuthenticated],
            authentication_classes=[TokenAuthentication])
    def logout(self, request, *args, **kwargs):
        token = Token.objects.get(user=request.user)
        token.delete()
        return Response({})


def missing_fields(data: dict, *fields):
    for field in fields:
        if not data.get(field, None):
            return True, field
    return False, None
