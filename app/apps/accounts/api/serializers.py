from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers


class AccountsSerializer(serializers.ModelSerializer):
    """Detail serializer of User objetc"""

    class Meta:
        model = get_user_model()
        fields = ['email', 'name', 'password', 'id']
        extra_kwargs = {'password': {'write_only': True,
                                     'validators': [validate_password]},
                        'id': {'read_only': True}
                        }

    def create(self, validated_data):
        """Create and return a new user"""
        user = get_user_model().objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        if password:
            instance.set_password(password)
        return super().update(instance, validated_data)
