from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


class AccountsRegisterForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ['email', 'name', 'password1', 'password2', ]


class EditUserProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["name", 'email']
        labels = {"name": "Nome"}
        widgets = dict(
            name=forms.TextInput(attrs={"class": "form-control"}),
            email=forms.TextInput(attrs={"class": "form-control"})
        )
