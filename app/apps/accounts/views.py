from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import UpdateView, CreateView

from .forms import AccountsRegisterForm, EditUserProfileForm


class RegisterUserView(CreateView):
    form_class = AccountsRegisterForm
    success_url = 'login'
    template_name = 'accounts/register.html'


class UserLoginView(LoginView):
    template_name = 'accounts/login.html'
    redirect_authenticated_user = True


class UserLogoutView(LogoutView):
    next_page = 'index'


class EditProfileView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login'

    model = get_user_model()
    template_name = 'accounts/edit_profile.html'
    form_class = EditUserProfileForm
    success_url = 'edit-profile'

    def get_object(self, queryset=None):
        return self.request.user
