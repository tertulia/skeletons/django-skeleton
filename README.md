# TERTÚLIA DJANGO SKELETON

Support us

<a href="https://www.paypal.com/donate?hosted_button_id=82Z99Y5ZNZNT2&source=url">
  <img src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" alt="Donate with PayPal" />
</a>

## About

This repository aim aggregate many skeletons modules as possible. You can use these skeletons as long as you follow
the terms of the [LICENSE](LICENSE).

## Best Practices

- Use of flake8 to pattern the code
- Test of modules and 100% coverage (Test-Driven Development is recommended)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## How contribute

### Making a suggestion

If you want to suggest a new module go to the `issues` in the side menu and create a new issue with the
`new_module` template, then fill the form with a good description about the module.

### Creating module

To create a new module to the project make first a suggestion, just like the previous step. After the discussion and the
approval, we will create a branch and a merge request for you to work on this module. When finished, check the merge
request as ready. If everything is following good practices, your module is integrated to the project.

### Report a bug

If you want to report a bug go to the `issues` in the side menu and create a new issue with the
`fix` template, then fill the form with a good description about this bug

### Add a feature

If you want to add a new feature go to the `issues` in the side menu and create a new issue with the
`feat` template, then fill the form with a good description about this feature

### Refactor a module

If you want to make even better a module go to the `issues` in the side menu and create a new issue with the
`refactor` template, then fill the form with a good description about this enhancement

### Suggest or add a test

If you want add a test or think that something must be tested, go to the `issues` in the side menu and create a new
issue with the
`test` template, then fill the form with a good description about this test

# How use
## For development
### Installing dependencies
#### Virtual environment 

After clone the project, create the virtualenv 

```bash

 $ virtualenv venv -p python

```

To active the venv

- linux `source venv/bin/activate`

- windows `venv\Scripts\activate`

To install dependecies
```
(venv) $ pip install -r app/environments/requirements-dev.txt 
```

### Apply migrations
To apply migrations use the command
```bash
(venv) $ python app/manage.py migrate
```

Or use Makefile
```bash
(venv) $ make migrate
```

#### Run

```bash
(venv) $ python manage.py runserver
```

Or use Makefile
```bash
(venv) $ make runserver
```

## For production
For production we are usign docker-compose, but feel free to use docker por development.
The production environment has based on [this tuttorial](https://www.capside.com/labs/deploying-full-django-stack-with-docker-compose/), we strongly recommend you to read it.

## For yourself

In case that you want use some, or all, modules, just create a fork. We will appreciate :smile:

### Check the TODO in the code to personalize your project as you want
